#include <stdio.h>
#include <stdlib.h>
#include <ctime>

const int LIMIT_WEZLOW = 500000;
const int LIMIT_SAMOCHODOW = 500000;

int megarand(int modile)
{
	return abs((rand() * rand() * rand()) % modile);
}

int main()
{
	srand(time(NULL));
	int liczba_wezlow = megarand(LIMIT_WEZLOW) + 1;
	int liczba_pojazdow = megarand(LIMIT_SAMOCHODOW) + 1;
	printf("%i %i\n", liczba_wezlow, liczba_pojazdow);
	scanf("%i%i", &liczba_wezlow, &liczba_pojazdow);
	for (int i = 1; i < liczba_wezlow; i++) {
		printf("%i %i\n", i, megarand(i));
	}
	for (int i = 1; i < liczba_pojazdow; i++) {
		int a = megarand(liczba_wezlow);
		int b;
		do {
			b = megarand(liczba_wezlow);
		} while (b == a);
		printf("%i %i %i\n", a, b, megarand(LIMIT_SAMOCHODOW));
	}

}
