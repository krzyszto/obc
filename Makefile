SRC= obc.cpp
BIN=obc
CXXFLAGS= -std=c++11 -O2 -ggdb3 -Wall
CXX=g++
all: $(BIN) gentest

$(BIN): $(SRC)
	$(CXX) $(CXXFLAGS) $(SRC) -o $(BIN)
gentest: gentest.cpp
	$(CXX) $(CXXFLAGS) gentest.cpp -o gentest
clean:
	rm $(BIN)
