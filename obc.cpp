#include <stdio.h>
#include <algorithm>
#include <set>
#include <vector>
#include <math.h>
#include <limits.h>

struct Wezel{
	std::vector<int> dzieci;
	long long int wartosc;
	int *przodkowie;
	int glebokosc;
	int pre_order;
	int liczba_wezlow_pode_mna;
	~Wezel();
	Wezel(const int indeks_ojca, const int moj_indeks,
		     std::vector<std::pair<int, int> > *krawedzie,
		     unsigned int &pre_order);
	int get_rozmiar_przodkow();
};


const int nil = -1;
const int PODSTAWA_LOGARYTMU = 2;
const int PODSTAWA_POTEGI = 2;

Wezel **drzewo = nullptr;
long long unsigned int liczba_wezlow;


// Daje iterator wektora, ktory obecnie wskazuje na wszystkie
// krawedzie wychodzace z indeks_wierzcholka.
std::vector<std::pair<int, int> >::iterator
get_iterator_krawedzi(std::vector<std::pair<int, int> > *graf,
			   const int indeks_wierzcholka)
{
	return upper_bound(graf->begin(), graf->end(),
			   std::make_pair(indeks_wierzcholka - 1, INT_MAX));
}

// Funkcja dajaca sufit z logarytmu dwojkowego z a.
int log_sufit(const int a)
{
	double doubleresult = log(a) / log(PODSTAWA_LOGARYTMU);
	int result = ceil(doubleresult);
	if (a < 2) {
		result = 1;
	}
	// printf("log_sufit dla %i to %i\n", a, result);
	return result;
}

// Funkcja dajaca 2 do potegi podanej jako argument.
int potega(const int wykladnik)
{
	return floor(pow(PODSTAWA_POTEGI, wykladnik));
}

// Destruktor wezla, usuwa wszystkie obiekty zaalokowane na stercie.
Wezel::~Wezel()
{
	delete[] this->przodkowie;
}

// Funkcja podajaca rozmiar tablicy przodkow danego wezla.
int Wezel::get_rozmiar_przodkow()
{
	if (this->glebokosc == 0) {
		return 1;
	}
	int result = log_sufit(this->glebokosc) + 1;
	if (result < 0)
		result = 0;
	return result;
}

// Funkcja sprawiajaca, ze ostatni_praszczur zamiast wskazywac wierzcholek,

// ktory obecnie wskazuje bedzie wskazywal wierzcholek o ile wyzej.
void rusz_sie_do_gory(int ile, int &ostatni_praszczur)
{
	while ((ile > 0) && (ostatni_praszczur != 0)) {
		ostatni_praszczur = drzewo[ostatni_praszczur]->przodkowie[0];
		ile--;
	}
}

// Funkcja sprawdzajaca, czy brat ma juz zrobiona tablice przodkow.

// Jesli tak, przepisuje cala tablice brata do swojej.
bool przepisz_od_brata(const int indeks)
{
	if (indeks == 0) {
		drzewo[indeks]->przodkowie[0] = -1;
		return true;
	}
	Wezel *ojciec = drzewo[drzewo[indeks]->przodkowie[0]];
	int brat = *(ojciec->dzieci.begin());
	if (brat == indeks)
		return false;
	for (int i = 0; i < drzewo[indeks]->get_rozmiar_przodkow(); i++) {
		drzewo[indeks]->przodkowie[i] = drzewo[brat]->przodkowie[i];
	}
	return true;
}


// Konstruktor wezla.
// Tu sie dzieje cala magia.
// Konstruktor wypenila nastepujace pola:
// -tablica przodkow
// -numer w porzadku pre order
// -liczbe wezlow w drzewie, ktorego dany wierzcholek jest korzeniem.
// -glebokosc
// -dzieci

// Tworzy takze swoje dzieci.
// Konstruktor rekurencyjny.
Wezel::Wezel(const int indeks_ojca, const int moj_indeks,
	     std::vector<std::pair<int, int> > *krawedzie,
	     unsigned int &pre_order)
{
	this->wartosc = 0;
	this->przodkowie = nullptr;
	drzewo[moj_indeks] = this;
	this->pre_order = pre_order;
	pre_order++;
	if (indeks_ojca != nil) {
		this->glebokosc = drzewo[indeks_ojca]->glebokosc + 1;
	} else {
		this->glebokosc = 0;
	}
	this->przodkowie = new int[get_rozmiar_przodkow()];
	this->przodkowie[0] = indeks_ojca;
	for (std::vector<std::pair<int, int> >::iterator it =
		     get_iterator_krawedzi(krawedzie, moj_indeks);
	     it < krawedzie->end() && it->first == moj_indeks; it++) {
		if (drzewo[it->second] == nullptr) {
			this->dzieci.push_back(it->second);
		}
	}
	for (auto a : this->dzieci) {
		if (drzewo[a] == nullptr) {
			drzewo[a] = new Wezel(moj_indeks, a,
					      krawedzie, pre_order);
		}
	}
	if (this->dzieci.empty()) {
		if (!przepisz_od_brata(moj_indeks)) {
			int ostatni_praszczur = moj_indeks;
			for (int i = 1; i < get_rozmiar_przodkow(); i++) {
				rusz_sie_do_gory(potega(i), ostatni_praszczur);
				this->przodkowie[i] = ostatni_praszczur;
			}
		}
	} else {
		// To tylko po to, zeby nie robic
		// dluuugich, zagniezdzonych linii i wielowarstwowych odwolan.
		int indeksSyna = *dzieci.begin();
		Wezel *syn = drzewo[indeksSyna];
		int *przodkowieSyna = syn->przodkowie;
		for (int i = 1; i < get_rozmiar_przodkow(); i++){
			int indeksJedenWyzej;
			if (przodkowieSyna[i] == -1) {
				indeksJedenWyzej = -1;
			} else {
				indeksJedenWyzej =
					std::max(0,
						 drzewo[przodkowieSyna[i]]->przodkowie[0]);
			}
			przodkowie[i] = indeksJedenWyzej;
		}
	}
	this->liczba_wezlow_pode_mna = pre_order - this->pre_order;
}

// Nie testowane, ale jezeli pola sa dobrze wypelnione to powinno dzialac.
bool jest_potomkiem(const int ojciec, const int syn)
{
	if (ojciec == nil || syn == nil) {
		return false;
	}
	if (ojciec == 0) {
		return true;
	}
	if (ojciec == syn) {
		return true;
	}
	if (syn == 0) {
		return false;
	}
	return (drzewo[syn]->pre_order >= drzewo[ojciec]->pre_order) &&
		(drzewo[syn]->pre_order < drzewo[ojciec]->pre_order +
		 drzewo[ojciec]->liczba_wezlow_pode_mna);
}

// Podaje indeks przodka, ktory jest 2^potega przed wskazanym wierzcholkiem.
int indeks_przodka(const int wierzcholek, const int potega)
{
	if (potega >= drzewo[wierzcholek]->get_rozmiar_przodkow())
		return 0;
	return drzewo[wierzcholek]->przodkowie[potega];
}

// Podaje najnizszego wspolnego przodka wierzcholkow a i b.
int LCA(const int a, const int b)
{
	// b jest potomkiem a.
	if (jest_potomkiem(a, b)) {
		return a;
	}
	// a jest potomkiem b.
	if (jest_potomkiem(b, a)) {
		return b;
	}
	// skoczna puma.
	int i = a;
	int j = log_sufit(liczba_wezlow);
	while (j >= 0) {
		// indeks_przodka najpierw przyjmuje wierzcholek,
		// potem wykladnik potegi.
		int c = indeks_przodka(i, j);
		// jest_potomkiem(ojciec, syn)
		if (jest_potomkiem(c, b)) {
			j--;
		} else {
			i = c;
		}
	}
	return indeks_przodka(i, 0);
}

void wyznacz_maksa(const int indeks, long long int &rekord)
{
	for (auto a: drzewo[indeks]->dzieci) {
		wyznacz_maksa(a, rekord);
	}
	for (auto a: drzewo[indeks]->dzieci) {
		drzewo[indeks]->wartosc += drzewo[a]->wartosc;
	}
	rekord = std::max(rekord, drzewo[indeks]->wartosc);
}

int main() {
	std::vector<std::pair<int, int> > *krawedzie;
	long long unsigned int liczbaPojazdow;
	scanf("%llu%llu", &liczba_wezlow, &liczbaPojazdow);
	krawedzie = new std::vector<std::pair<int, int> >;
	for (long long unsigned int i = 0; i < liczba_wezlow - 1; i++) {
		std::pair<int, int> para;
		int a, b;
		scanf("%i%i", &a, &b);
		para.first = std::min(a, b);
		para.second = std::max(a, b);
		krawedzie->push_back(para);
		krawedzie->push_back(std::make_pair(para.second, para.first));
	}
	krawedzie->shrink_to_fit();
	std::sort(krawedzie->begin(), krawedzie->end());
	// wyglada na to, ze do tego miejsca jest w porzadalu.
	drzewo = new Wezel*[liczba_wezlow];
	std::fill(drzewo, drzewo + liczba_wezlow, nullptr);
	unsigned int pre_order = 0;
	drzewo[0] = new Wezel(nil, 0, krawedzie, pre_order);
	delete krawedzie;
	// Zapamietywanie przejazdow samochodow.
	for (long long unsigned int i = 0; i < liczbaPojazdow; i++) {
		unsigned int a, b, sam;
		scanf("%i%i%i", &a, &b, &sam);
		drzewo[a]->wartosc += sam;
		drzewo[b]->wartosc += sam;
		drzewo[LCA(a, b)]->wartosc -= sam * 2;
	}
	long long int rekord = 0;

	wyznacz_maksa(0, rekord);

	for (unsigned int i = 0; i < liczba_wezlow; i++) {
		delete drzewo[i];
	}
	delete[] drzewo;

	printf("%lli\n", rekord);

	return 0;
}
